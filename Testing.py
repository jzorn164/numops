#Test Suite for Numerical Optimization Module

#Import Modules
#---------------------------------
import sympy as sp
import numericaloptimization as no
import textwrap, time
#---------------------------------

#Code
#---------------------------------
str_to_print = 'Welcome to the Numerical Optimization Module Created by Jacob Zorn, of Penn State University.'
str_to_print += ' Within this module you will found a wide variety of Numerical Optimization Algorithms that you '
str_to_print += 'may find helpful for research, teaching, and exploration. This is the test suite of available methods '
str_to_print += 'which you may find helpful to get started.'

print('------------------------------------------------------------')
for text in (textwrap.wrap(str_to_print,width=60)):
	print(text)
print('------------------------------------------------------------')

print('------------------------------------------------------------')
print('Beginning Bisection Method Testing')

x 			= sp.Symbol('x')
SymProb 	= 4*x - 15
lambda_Prob = sp.lambdify(x,SymProb,'numpy')
start_time 	= time.time()
Root		= no.General_Optimization.Bisection_Method(lambda_Prob, -10, 10, tol=1e-6, Find_Value=0, Args='')
print('Found {} in only {:8.7f} Seconds'.format(Root,time.time() - start_time))
print('')
print('Pretty quick right. Let us try another method.')

print('------------------------------------------------------------')
print('Beginning Newton Raphson Method Testing')
start_time 	= time.time()
deriv_Prob	= sp.lambdify(x,SymProb.diff(x),'numpy')
Root		= no.General_Optimization.Newton_Raphson(lambda_Prob, deriv_Prob, -10, 0, tol=1e-6, Args='', max_iters=2000)
print('Found {} in only {:8.7f} Seconds'.format(Root,time.time() - start_time))
print('')
print('------------------------------------------------------------')

str_to_print = 'Now that we have showed you a few simple root finding methods, let us'
str_to_print += ' now move to something more complicated, global non-linear optimization.'
str_to_print += ' We will begin by minimizing a very common problem, the Rosenbrock Function.'
str_to_print += ' It is encouraged that you check out the shape of the curve, online or using your favorite visualization software.'

for text in (textwrap.wrap(str_to_print,width=60)):
	print(text)

print('------------------------------------------------------------')
print('Beginning Differential Evolution Test Suite')

x, y = sp.symbols('x y')
SymProb 	= 100 * (y - x**2)**2 + (1 - x)**2
Bounds 		= [(-10,10),(-10,10)]
lambda_Prob = sp.lambdify([x,y],SymProb,'numpy')
start_time	= time.time()
result, Vector, Fitness, Iterations = no.Differential_Evolution.Differential_Evolution(lambda_Prob, Bounds, max_iters=2000, tol=1e-10, popsize=50, crosspop=0.8, mutation=0.6, strategy='rand1bin', Args='')

str_to_print = ('The Fitness Value is {:6.5e}, the Decision Vector is {}, and the Total Time was {:8.7f} Seconds'.format(Fitness, Vector, time.time() - start_time))
for text in (textwrap.wrap(str_to_print,width=60)):
	print(text)
print('')

print('------------------------------------------------------------')

str_to_print = 'What about if we have a constrained optimization problem. How could '
str_to_print += 'or could we solve that type of problem. Well for starters there is a'
str_to_print += ' constrained differential evolution solver built into the module. So '
str_to_print += 'using problem G03 from the 2013 CEC Problem set, lets see how we do.'

for text in (textwrap.wrap(str_to_print,width=60)):
	print(text)
print('------------------------------------------------------------')
print('Beginning Constrained Differential Evolution')

functionName = 'G06'
   
#G11 Works
if functionName == 'G11':
	x, y = sp.symbols('x y')
	variables = [x,y]
	prob = x**2 + (y-1)**2
	Con1 = y - x**2
	
	bounds = [(-0.9,0.9), (-0.9,0.9)]
	Constraints = [Con1 - 0.0001]

#G06 not quite
if functionName == 'G06':
	x, y  = sp.symbols('x y')
	variables = [x,y]
	prob = (x - 10)**3 + (y - 20)**3
	Con1 = -(x - 5)**2 - (y - 5)**2 + 100
	Con2 = (x - 6)**2 + (y - 5)**2 - 82.81
	
	bounds = [(13,15), (0,1)]
	Constraints = [Con1, Con2]

#G03 (2Dim Works)
if functionName == 'G03':
		dims = 10
		variables = sp.symbols(['x_{}'.format(i) for i in range(dims)])
		prob = 1
		for i in variables:
			prob *= i
		prob = prob * -((dims)**0.5)**dims
		
		Con1 = 0
		for i in variables:
			Con1 += i**2
		   
		bounds = []
		for i in range(dims):
			bounds.append((0,1))
		
		Constraints = [Con1-1-0.0001]
#G01 Works
if functionName == 'G01':
	
	variables = sp.symbols(['x_{}'.format(i) for i in range(1,14)])
	prob = 0
	for i in range(1,5):
		prob += 5 * variables[i-1]

	for i in range(1,5):
		prob += -5*variables[i-1]**2

	for i in range(5,14):
		prob += -variables[i-1]

	Con1 = 2 * variables[0] + 2 * variables[1] + variables[9] + variables[10] - 10
	Con2 = 2 * variables[0] + 2 * variables[2] + variables[9] + variables[11] - 10
	Con3 = 2 * variables[2] + 2 * variables[1] + variables[10] + variables[11] - 10
	Con4 = -8 * variables[0] + variables[9]
	Con5 = -8 * variables[1] + variables[10]
	Con6 = -8 * variables[2] + variables[11]
	Con7 = -2 * variables[3] - variables[4] + variables[9]
	Con8 = -2 * variables[5] - variables[6] + variables[10]
	Con9 = -2 * variables[7] - variables[8] + variables[11]

	Constraints = [Con1, Con2, Con3, Con7, Con8, Con9]
	#Constraints = [Con1, Con2, Con3, Con4, Con5, Con6, Con7, Con8, Con9]
	bounds = [(0,1), (0,1), (0,1), (0,1), (0,1), (0,1), (0,1), (0,1), (0,1), (0,100), (0,100), (0,100), (0,1)]

temp_con = []
temp_type = []
for Con in Constraints:
	temp_con.append(sp.lambdify(variables, Con))
	temp_type.append('NotEq')
	
Constraint_Dict = {'ConstraintEqs':temp_con,'Type':temp_type}

Population, Fitness, Constraint = no.Differential_Evolution.Constrained_Differential_Evolution(sp.lambdify(variables,prob,'numpy'), bounds, Constraint_Dict, max_iters=500, tol=1e-6, popsize=50, crosspop=0.8, mutation=0.6, strategy='rand1bin', Args='')
print('------------------------------------------------------------')

str_to_print	= 'How about cases of multimodal optimization? Let us go ahead and optimize the'
str_to_print	+= ' common 6-Hump Camel Back Function, to see if we can arrive at the proper '
str_to_print	+= 'minima points.'

for text in (textwrap.wrap(str_to_print,width=60)):
	print(text)
print('------------------------------------------------------------')
print('Beginning Multimodal Differential Evolution')

x, y = sp.symbols('x y')
Prob = (4 - 2.1*x**2 + (x**4)/3)*x**2 + x*y + (-4 + 4*y**2)*y**2
ymin = -1.1 ; ymax = 1.1
xmin = -1.9 ; xmax = 1.9
prob_roots = 2
local_roots = 4
Prob = sp.lambdify([x,y],Prob,'numpy')
Bounds = [(xmin,xmax),(ymin,ymax)]

result, roots = no.Differential_Evolution.Multimodal_Differential_Evolution(Prob, Bounds, max_iters=1000, tol=1e-6, popsize=50, crosspop=0.8, mutation=0.6, strategy='currentRand1bin', Args='')
for root, i in zip(roots,range(1,len(roots)+1)):
	print('Found Root #{}: {}'.format(i,root))