"""
Numerical Optimization Library
Written by Jacob Zorn
Date Created: 11/5/2019
Date Updated: 1/27/2020
Version: 0.2.0
"""

#Import Outside Libraries 
import numpy as np
import sys
import numdifftools as nd
import matplotlib.pyplot as plt
import itertools as itools

#-------------------------------

#Classes

class General_Routines:
	
	def Check_Args(Prob, Args, Bounds):
		
		if type(Args) is list:
			args_count = len(Args) + len(Bounds)
		else:
			args_count = len(Bounds)
			
		prob_parameters = Prob.__code__.co_argcount
		
		if prob_parameters != args_count:
			sys.exit('The number of parameters is not consistent. Quitting Optimization, please address in your problem statement.')
			
	def Create_Vector(Bounds):
		
		min_b, max_b 	= np.asarray(Bounds).T
		return (min_b + np.random.rand(1,len(Bounds)) * np.fabs(min_b - max_b))[0]
		
	def Calculate_Fitness(Vector, Prob, Args=''):
		
		if Args == '':
			return Prob(*Vector)
		else:
			return Prob(*np.concatenate((Vector,Args)))
			
	def Population_Setup(Popsize, Bounds):
		
		Population		= np.random.rand(Popsize, len(Bounds))
		min_b, max_b	= np.asarray(Bounds).T
		return min_b + Population * np.fabs(min_b - max_b)
		
	def Elitest_Selection(fV1, fV2):
		
		if fV1 < fV2:
			return True
		else:
			return False
	
	def Strategy_Selection(Population, Mutation, Current, Best, avail_idxs, Strategy='rand1bin'):
	
		#If performing Multimodal, choose currentRand1bin, currentRand2bin, best1bin, best2bin, and replace either best or current with the nearest neighbor
		
		a,b,c,d,e = Population[np.random.choice(avail_idxs, 5, replace=False)]
		
		if Strategy is 'rand1bin':
			Mutant = a + Mutation * (b-c)
		elif Strategy is 'rand2bin':
			Mutant = a + Mutation * (b - c + d - e)
		elif Strategy is 'best1bin':
			Mutant = Best + Mutation * (b - c)
		elif Strategy is 'best2bin':
			Mutant = Best + Mutation * (b - c + d - e) 
		elif Strategy is 'currentBest1bin':
			Mutant = Current + Mutation * (Best - Current + b - c)
		elif Strategy is 'currentBest2bin':
			Mutant = Current + Mutation * (Best - Current + b - c + d - e)
		elif Strategy is 'bestCurrent1bin':
			Mutant = Best + Mutation * (Current - b)
		elif Strategy is 'bestCurrent2bin':
			Mutant = Best + Mutation * (Current - b + d - e)
		elif Strategy is 'currentRand1bin':
			Mutant = Current + Mutation * (b - c)
		elif Strategy is 'currentRand2bin':
			Mutant = Current + Mutation * (b - c + d - e)
		elif Strategy is 'randCurrent1bin':
			Mutant = a + Mutation * (Current - c)
		elif Strategy is 'randCurrent2bin':
			Mutant = a + Mutation * (Current - c + d - e)
		
		return Mutant
		
	def Mutant_Crossover(CrossPollinator, Current, Mutant, Bounds):
		
		for i in range(len(Bounds)):
			Mutant[i] = np.clip(Mutant[i],Bounds[i][0],Bounds[i][1])
		
		cross_points	= np.random.rand(len(Bounds)) < CrossPollinator
		cross_points[np.random.randint(0,len(Bounds))] = True
		Trial = np.where(cross_points, Mutant, Current)
		return Trial
	
	def Gradient_Assist(Gradient, Hessian, Scaling, Vector, Bounds, Args=''):
		
		array_Hessian 	= np.zeros((len(Hessian),len(Hessian[0])))
		array_Gradient 	= np.zeros((len(Gradient),1)) 
		
		if Args == '':
			for i, j in itools.product(range(len(Hessian)),range(len(Hessian[0]))):
				array_Hessian[i,j] 	= Hessian[i][j](*Vector)
			for i in range(len(Hessian)):
				array_Gradient[i]	= Gradient[i](*Vector) 
		else:
			for i, j in itools.product(range(len(Hessian)),range(len(Hessian[0]))):
				array_Hessian[i,j] 	= Hessian[i][j](*np.concatenate((Vector,Args)))
			for i in range(len(Hessian)):
				array_Gradient[i]	= Gradient[i](*np.concatenate((Vector,Args)))

		vec_adjust 	= np.linalg.solve(array_Hessian, -1 * array_Gradient)
		
		Trial = Vector + Scaling * vec_adjust
		for i in range(len(Bounds)):
			Trial[i] = np.clip(Trial[i],Bounds[i][0],Bounds[i][1])
		
		return Trial
	
	def Calculate_Distance(Current, Population):
		NearDist	= 100
		for idx in Population:
			dist 	= np.linalg.norm(Current - idx)
			if dist < NearDist and dist != 0:
				NearNeigh 	= idx
				NearDist	= dist
		
		return NearNeigh
	
	def Fitness_Test(Fitness1, Fitness2):
		if (Fitness1 <= Fitness2):
			return True
		else:
			return False

	def Constraint_Test(Fitness1, Fitness2, Penalty1, Penalty2, Epsilon):
		#Return True if Replacing Fitness2 with Fitness1
		
		if (Penalty1 < Epsilon and Penalty2 < Epsilon):
			return General_Routines.Fitness_Test(Fitness1, Fitness2)
		elif (Penalty1 == Penalty2):
			return General_Routines.Fitness_Test(Fitness1, Fitness2)
		elif (Penalty1 < Penalty2):
			return True
		else:
			return False
			
	def Penalty_Elitest_Archive(Population, Penalty, Portion):
		sorted_Pen = Penalty.argsort()
		Archive = []
		for i in range(round(len(Population)*Portion)):
			Archive.append(Population[sorted_Pen[i]].tolist())
			
		return np.asarray(Archive)
	
class Differential_Evolution:
	
	def Differential_Evolution(Prob, Bounds, max_iters=2000, tol=1e-6, popsize=50, crosspop=0.8, mutation=0.6, strategy='rand1bin', Args=''):
		
		General_Routines.Check_Args(Prob, Args, Bounds)
		
		Population		= General_Routines.Population_Setup(popsize, Bounds)
		Fitness 		= np.asarray([General_Routines.Calculate_Fitness(idx, Prob, Args=Args) for idx in Population])
		
		iteration 		= 0
		error			= 1.0
		
		Best_IDX 		= np.argmin(Fitness)
		Best			= Population[Best_IDX]
		Best_Fit		= Fitness[Best_IDX]
		
		while iteration < max_iters and error > tol:
			for idx in range(popsize):
				Current 	= Population[idx]
				avail_idxs	= [jidx for jidx in range(popsize) if jidx != idx]
				
				#Mutation Strategy
				Mutant 			= General_Routines.Strategy_Selection(Population, mutation, Current, Best, avail_idxs, Strategy=strategy)
				
				#Crossover
				Trial_Vec		= General_Routines.Mutant_Crossover(crosspop, Current, Mutant, Bounds)
				
				#Fitness
				Trial_Fitness	= General_Routines.Calculate_Fitness(Trial_Vec, Prob, Args=Args)
				
				#Comparsion
				flag_Elite		= General_Routines.Elitest_Selection(Trial_Fitness, Fitness[idx])
				
				#True means replace Trial_Vec with Current_Vec
				if flag_Elite:
					Fitness[idx]	= Trial_Fitness
					Population[idx]	= Trial_Vec
					flag_Best		= General_Routines.Elitest_Selection(Trial_Fitness, Best_Fit)
					if flag_Best:
						Best_IDX	= idx
						error		= np.abs(Trial_Fitness - Best_Fit)
						Best		= Trial_Vec
						Best_Fit	= Trial_Fitness
						if error < tol:
							return 'Optimal Solution', Best, Best_Fit, iteration+1
						
			iteration += 1
		
		return 'Non-Optimal Solution', Best, Best_Fit, iteration + 1
		
	def Multimodal_Differential_Evolution(Prob, Bounds, max_iters=2000, tol=1e-6, popsize=50, crosspop=0.8, mutation=0.6, strategy='rand1bin', Args=''):
		
		General_Routines.Check_Args(Prob, Args, Bounds)
		
		Population		= General_Routines.Population_Setup(popsize, Bounds)
		Fitness 		= np.asarray([General_Routines.Calculate_Fitness(idx, Prob, Args=Args) for idx in Population])
		
		iteration 		= 0
		error			= 1.0
		Roots			= []
		dTol			= 1e-6
		
		Best_IDX 		= np.argmin(Fitness)
		Best			= Population[Best_IDX]
		Best_Fit		= Fitness[Best_IDX]
		
		while iteration < max_iters:
			for idx in range(popsize):
				Current 	= Population[idx]
				NearNeigh	= General_Routines.Calculate_Distance(Current, Population)
				avail_idxs	= [jidx for jidx in range(popsize) if jidx != idx]
				
				#Mutation Strategy
				Mutant			= General_Routines.Strategy_Selection(Population, mutation, NearNeigh, Best, avail_idxs, Strategy=strategy)
				
				#Crossover
				Trial_Vec		= General_Routines.Mutant_Crossover(crosspop, Current, Mutant, Bounds)
				
				#Fitness
				Trial_Fitness	= General_Routines.Calculate_Fitness(Trial_Vec, Prob, Args=Args)
				
				#Comparsion
				flag_Elite		= General_Routines.Elitest_Selection(Trial_Fitness, Fitness[idx])
				
				#Replacement
				if flag_Elite:
					Fitness[idx]	= Trial_Fitness
					Population[idx]	= Trial_Vec
					flag_Best		= General_Routines.Elitest_Selection(Trial_Fitness, Best_Fit)
					if flag_Best:
						Best_IDX	= idx
						error		= np.abs(Trial_Fitness - Best)
						Best		= Trial_Vec
						Best_Fit	= Trial_Fitness
						
				#Root Collecting
				dError			= 0.0
				adjust_Matrix	= np.zeros((len(Bounds),len(Bounds)))
				for i in range(len(Bounds)):
					adjust_Matrix[i,i]	= 1e-10
					dTrial				= Trial_Vec + np.matmul(adjust_Matrix, Trial_Vec)
					adjust_Matrix[i,i] 	= 0.0
					dError				+= abs(General_Routines.Calculate_Fitness(Trial_Vec, Prob, Args=Args) - General_Routines.Calculate_Fitness(dTrial, Prob, Args=Args)) / np.linalg.norm(dTrial - Trial_Vec)
				
				if dError < dTol:
					flag_Add			= True
					if len(Roots) > 0:
						for root in Roots:
							error_Root		= np.linalg.norm(root - Trial_Vec)
							if error_Root < 1e-1:
								Trial_Vec		= General_Routines.Create_Vector(Bounds)
								Trial_Fitness	= General_Routines.Calculate_Fitness(Trial_Vec, Prob, Args=Args)
								Fitness[idx]	= Trial_Fitness
								Population[idx]	= Trial_Vec
								flag_Add		= False
					if flag_Add:
						print('Adding Root: {}'.format(Trial_Vec))
						Roots.append(Trial_Vec)
						Trial_Vec		= General_Routines.Create_Vector(Bounds)
						Trial_Fitness	= General_Routines.Calculate_Fitness(Trial_Vec, Prob, Args=Args)
						Fitness[idx]	= Trial_Fitness
						Population[idx]	= Trial_Vec
						flag_Add		= False
			
			iteration += 1
			
		return 'Reached Max Iterations', Roots
	
	def Constrained_Differential_Evolution(Prob, Bounds, Constraint_Dict, max_iters=2000, tol=1e-6, popsize=50, crosspop=0.6, mutation=0.7, strategy='rand1bin', Args=''):
	
		General_Routines.Check_Args(Prob, Args, Bounds)
		
		Relaxed_Gen		= round(0.2 * max_iters)
		Theta			= 0.20
		Regens			= 3
		iteration		= 0
		
		Population		= General_Routines.Population_Setup(popsize, Bounds)
		Fitness 		= np.asarray([General_Routines.Calculate_Fitness(idx, Prob, Args=Args) for idx in Population])
		Penalty			= np.asarray([[abs(General_Routines.Calculate_Fitness(idx, Con, Args=Args)) for Con in Constraint_Dict['ConstraintEqs']] for idx in Population]).sum(1)
		
		Elitest_Archive	= General_Routines.Penalty_Elitest_Archive(Population, Penalty, Theta)
		
		eps0			= np.sort(Penalty)[round(Theta * popsize)]
		eps				= eps0
		
		while iteration < max_iters:
			for idx in range(popsize):
				Current		= Population[idx]
				Pop_Arch	= np.concatenate((Population, Elitest_Archive),axis=0)
				
				avail_idxs	= [jidx for jidx in range(len(Pop_Arch)) if idx != jidx]
				
				Current_Fitness		= Fitness[idx]
				Current_Penalty		= Penalty[idx]
				
				fGenerate	= True
				count_Gen	= 0
				
				while fGenerate and count_Gen < Regens:
					Mutant		= General_Routines.Strategy_Selection(Pop_Arch, mutation, Current, Current, avail_idxs, Strategy='rand1bin')
					Mutant		= General_Routines.Mutant_Crossover(crosspop, Current, Mutant, Bounds)
					Mutant_Fit	= General_Routines.Calculate_Fitness(Mutant, Prob, Args=Args)
					Mutant_Pen	= np.asarray([abs(General_Routines.Calculate_Fitness(Mutant, Con, Args=Args)) for Con in Constraint_Dict['ConstraintEqs']]).sum()
					if Mutant_Pen <= Current_Penalty:
						fGenerate = False
					count_Gen += 1
					
				fChange			= General_Routines.Constraint_Test(Mutant_Fit, Current_Fitness, Mutant_Pen, Current_Penalty, eps)
				
				if fChange:
					Fitness[idx]	= Mutant_Fit
					Penalty[idx]	= Mutant_Pen
					Population[idx]	= Mutant
				
			if iteration > Relaxed_Gen:
				eps					= np.sort(Penalty)[round(Theta * popsize)]
			else:
				eps					= eps0 * (1 - iteration / Relaxed_Gen)**5
			Elitest_Archive		= General_Routines.Penalty_Elitest_Archive(Population, Penalty, Theta)
			
			iteration			+= 1
			
		return Population, Fitness, Penalty
		
class General_Optimization:
	
	def Bisection_Method(Prob, Right, Left, tol=1e-6, Find_Value=0, Args=''):
		
		fRight 	= Prob(Right) - Find_Value
		fLeft	= Prob(Left) - Find_Value
		
		if (fRight * fLeft) > 0:
			sys.exit('The Bounds do not meet the requirements for the Bisection Method. fRight: {} and fLeft: {}'.format(fRight, fLeft))
			
		Error	= 1.0
		while Error > tol:
			xNew	= (Right + Left) / 2
			fNew	= Prob(xNew)- Find_Value
			
			if fRight * fNew < 0:
				Left 	= xNew
				fLeft	= fNew
			else:
				Right	= xNew
				fRight	= fNew
			
			Error = abs(fNew)
			
		return xNew
		
	def Newton_Raphson(Prob, Derivative, Inital, Find_Value, tol=1e-6, Args='', max_iters=2000):
		
		iteration 	= 0
		x			= Inital
		while iteration < max_iters:
			
			y 		= Prob(x)
			yprime	= Derivative(x)
			
			if abs(yprime) < 1e-14:
				break
				return x
			
			xNew	= x - y/yprime
			
			if abs(xNew - x) < tol:
				return xNew
			
			x		= xNew
			
		return x